#! /bin/bash

set -e

echo "Hello! It is now "`date '+%d %b %Y (%H:%M)'`

SCRIPT_LOC=$(dirname ${BASH_SOURCE[0]})
BASE=`realpath ${SCRIPT_LOC}`
OUTNAME=${1}
JOBTYPE=${2}
PHYSVAL_TOKEN=${3}
if [ ! -z "${PHYSVAL_TOKEN}" ]; then
  GITURL=https://oauth2:${PHYSVAL_TOKEN}@gitlab.com/sherpa-team/validation/sherpa-physval.git
else
  GITURL=git@gitlab.com:sherpa-team/validation/sherpa-physval.git
fi

# Print config info
rivet --version

nYODAS=$(ls | grep "Analysis_.*.yoda.gz" | wc -l)
if [[ "${nYODAS}" -gt 0 ]]; then
  CMD="rivet-merge --assume-reentrant -e -o ${OUTNAME}.yoda.gz Analysis_*.yoda.gz"
  echo ${CMD}
  $CMD
  cp ${OUTNAME}.yoda.gz ${BASE}/${JOBTYPE}/
  # add most relevant logs
  cp log_1.log ${BASE}/logs/${OUTNAME}_evgen.log
  cp log_merge.log ${BASE}/logs/${OUTNAME}_merge.log
  gzip ${BASE}/logs/${OUTNAME}_*.log
  cd ${BASE}
  git pull ${GITURL} --quiet 2>&1
  git add ${JOBTYPE}/${OUTNAME}.yoda.gz
  git add logs/${OUTNAME}_*.log.gz
  git commit -m"adding ${OUTNAME}" --quiet
  git push ${GITURL} $(git name-rev --name-only HEAD) --quiet
  git fetch --all --quiet || true
fi

echo "Done! It is now "`date '+%d %b %Y (%H:%M)'`
