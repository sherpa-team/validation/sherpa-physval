#!/usr/bin/bash -l

set -e

echo "Hello! It is now "`date '+%d %b %Y (%H:%M)'`

ENV=${1}
FORCE_BRANCH=${2}
PHYSVAL_TOKEN=${3}
FORCE_COMMIT=${4}
test -z "${NEW_REFS}" && NEW_REFS="0"
if [[ "${NEW_REFS}" -eq "1" ]]; then
  FORCE_BRANCH=master
fi
if [ ! -z "${PHYSVAL_TOKEN}" ]; then
  GITURL=https://oauth2:${PHYSVAL_TOKEN}@gitlab.com/sherpa-team/validation/sherpa-physval.git
fi

source ~/sherpa-physval/software/setup${ENV}
SCRIPT_LOC=$(dirname ${BASH_SOURCE[0]})
BASE=`realpath ${SCRIPT_LOC}`

function validate-commit() {

    TAG=${1} # tag/commit to be probed
    JOBTYPE=${2} # to distinguish feature branches from master
    PROCSET=${3} # optional argument to select subset of processes
    if [[ "${NEW_REFS}" -eq "1" ]]; then
      PROCSET="all"
    fi

    cd ${SHERPADIR}
    git checkout ${TAG} --quiet
    if [[ ! $TAG =~ ^v[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
      git reset --hard origin/${TAG} --quiet
    fi

    if [[ ${JOBTYPE} != *"tag"* ]]; then
      if [[ -z "${FORCE_COMMIT}" ]]; then
        TAG=${TAG}-$(git rev-parse --short=10 HEAD)
      else
        git checkout ${FORCE_COMMIT} --quiet
        TAG=${TAG}-${FORCE_COMMIT}
      fi
    fi
    SHERPA_BUILD="build-${TAG}"
    SHERPA_LOCAL="local-${TAG}"

    MERGEJOBS=""
    for row in `grep -v '^#' ${BASE}/procs.dat | grep ${ENV}`; do

      IFS=',' read -r -a arr <<< "$row"

      PROC=$(basename ${SHERPADIR}/${arr[1]})
      echo "Checking process \"${PROC}\" ..."
      if [[ "${NEW_REFS}" -eq "0" ]] &&
         [[ $(ls ${BASE}/${JOBTYPE}/ | grep ".*-${TAG}-${PROC}.yoda.gz" | wc -l) -ne 0 ]]; then
        continue # already ran this one ...
      fi

      if [[ -z "${PROCSET}" ]]; then
          # if the user goes with the default selection (empty PROCSET), skip MEPS@NLO
          if [[ ${PROC} == *"zjets"* ]] || [[ ${PROC} == *"ttjets"* ]]; then
            continue
          fi
      else
        # otherwise check whether process matches subset selection
        PASS=false
        for trig in $(echo ${PROCSET} | tr "," "\n"); do
          if [[ "${trig}" == "all" ]] || [[ "${PROC}" == *"${trig}"* ]]; then
            PASS=true
          fi
        done
        if [ "${PASS}" = false ] ; then
          continue # not requested
        fi
      fi
      echo "... accepted!"

      # build this commit
      if [ ! -d ${BASE}/software/sherpa/${SHERPA_BUILD} ]; then
        mkdir -p ${SHERPA_BUILD} && cd ${SHERPA_BUILD}
        echo "Configure ${TAG} ..."
        PHYSVAL_CONFIG
      fi
      if [ ! -d ${BASE}/software/sherpa/${SHERPA_LOCAL} ]; then
        echo "Build ${TAG} ..."
        cd ${BASE}/software/sherpa/${SHERPA_BUILD}
        PHYSVAL_INSTALL
      fi

      # extract job parameters
      NTASKS=${arr[2]}
      NHOURS=${arr[3]}
      VMEM=${arr[4]}

      JOBNAME=cron_${TAG}-${PROC}
      YODANAME=$(date '+%Y-%m-%d')-${TAG}-${PROC}

      # prepare job dir
      JOBDIR=${PHYSVAL_JOBLOC}/${JOBNAME}
      mkdir -p ${JOBDIR} && cd ${JOBDIR}

      # assemble run card path
      RUNCARD=${SHERPADIR}/${arr[1]}.yaml
      # copy runcard to dir
      if [[ -f "${RUNCARD}" ]]; then
          cp ${RUNCARD} ${JOBDIR}/${PROC}.yaml
      else
          echo "Could not find runcard: ${arr[1]}.yaml for tag/commit: ${TAG}"
          continue
      fi
      CONFIG=${PROC}.yaml

      # generate process libraries
      JOBID=$(PHYSVAL_MAKELIBS)
      echo "Submitted makelibs job ${JOBID}"

      # Submit array of evgen jobs
      JOBID=$(PHYSVAL_EVGEN)
      echo "Submitted generation job ${JOBID}"

      # Submit rivet-merge job
      JOBID=$(PHYSVAL_MERGE)
      echo "Submitted dependent merge job ${JOBID}"

      cd ${BASE}
      PLACEHOLDER=${BASE}/${JOBTYPE}/${YODANAME}.yoda.gz
      touch ${PLACEHOLDER}
      PHYSVAL_PUSH ${PLACEHOLDER} "add placeholder for ${JOBNAME}"

      MERGEJOBS="${MERGEJOBS}${MERGEJOBS:+,}${JOBID}"
    done

    # Submit clean-up job (only when all merge jobs are done)
    if [[ ! -z "${MERGEJOBS}" ]]; then
      JOBID=$(PHYSVAL_CLEANUP)
      echo "Submitted dependent clean-up job ${JOBID}"
    fi

    cd ${SHERPADIR}
    echo "${TAG} done!"
}


# See if there's any news ...
cd ${SHERPADIR}
git checkout ${FORCE_BRANCH:-master} --quiet

OLDHASH=$(ls -1S ${BASE}/refs-commits/*-${FORCE_BRANCH:-master}-*.yoda.gz | tail -n 1 | rev | cut -d "-" -f 2 | rev)
echo "Old commit hash: ${OLDHASH}"

DIGEST=$(git pull 2>&1)
#DIGEST=$(cat ${BASE}/test2.log)

NEWHASH=$(git rev-parse --short=10 HEAD)
echo "New commit hash: ${NEWHASH}"

if [[ ! -z "${FORCE_BRANCH}" ]]; then
  if [[ "${NEW_REFS}" -eq "0" ]] && [[ "${OLDHASH}" == "${NEWHASH}" ]]; then
    echo "Hash key unchanged: nothing to be done here. It is now "`date '+%d %b %Y (%H:%M)'`
  else
    validate-commit ${FORCE_BRANCH} refs-commits all
  fi
fi

if [[ "${NEW_REFS}" -eq "1" ]] || [[ -z "${FORCE_BRANCH}" ]]; then

  # Check for new tags
  TAGS=$(echo ${DIGEST} | grep "\[new tag\]" | cut -d "]" -f 2- | cut -d ">" -f 2 | xargs)
  if [[ "${NEW_REFS}" -eq "1" ]]; then
    # add all the existing tags starting with 3.0.0
    TAGS=($TAGS $(git tag | grep -v "v1" | grep -v "v2" | grep -v "alpha" | grep -v "beta"))
  fi
  # If there are new tags, validate them
  if [[ -z "${TAGS}" ]]; then
    echo "No new tags: nothing to be done here."
  else
    for TAG in ${TAGS[@]}; do
      echo "Submitting tag: $TAG"
      validate-commit ${TAG} refs-tags
    done
  fi

  # Check if feature branches were triggered
  BRANCHES=()
  git fetch --quiet --prune origin
  # Delete all local branches that are no longer on the remote
  DELS=$(git branch -r | awk '{print $1}' \
                | egrep -v -f /dev/fd/0 <(git branch -vv | grep origin) \
                | awk '{print $1}')
  if [[ ! -z "${DELS}" ]]; then
    git branch -D ${DELS} >/dev/null 2>&1
  fi
  for BRANCH in `git for-each-ref --format="%(refname)" refs/remotes/origin/`; do
    if [[ "${BRANCH}" == "refs/remotes/origin/master" ]] ||
       [[ ${BRANCH} == *"HEAD" ]] ||
       [[ ${BRANCH} == *"rel-1"* ]] ||
       [[ ${BRANCH} == *"rel-2"* ]]; then
      continue # skip master branch, and pre-Sherpa3 branches
    fi
    # get latest commit message, use regex to extract trigger message if it exist
    MSG=$(git log -n 1 --pretty=format:"%s" ${BRANCH} | sed -n "s/^.*\[\(physval\S*\)\].*$/\1/p")
    if [[ ! -z "${MSG}" ]]; then
      BRANCHES+=("${BRANCH}|${MSG}")
    fi
  done

  # If there was a trigger in the commit messages, do your thing
  if [[ "${NEW_REFS}" -eq "1" ]] || [[ -z "${BRANCHES}" ]]; then
    echo "No branches triggered: nothing to be done here."
  else
    for ITEM in ${BRANCHES[@]}; do
      BRANCH=$(echo ${ITEM} | cut -d "|" -f 1) # branch name to be validated
      BRANCH=${BRANCH##*/} # prune leading path segments
      # Check if an optional subset of procs was specified, e.g. "[physval=lep]"
      MSG=$(echo ${ITEM} | cut -d "|" -f 2- | sed -n "s/^.*=\(\S*\)$/\1/p")
      echo "Submitting: $BRANCH [$MSG]"
      validate-commit ${BRANCH} refs-commits ${MSG}
    done
  fi

fi
echo "All done! It is now "`date '+%d %b %Y (%H:%M)'`
