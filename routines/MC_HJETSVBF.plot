# BEGIN PLOT /MC_HJETSVBF/.*_log$
LogX=1
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/atlas_.*
Title=ATLAS phase space
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/incl_.*
Title=Inclusive phase space
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_pth.*
XLabel=$p_\mathrm{T}^H$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}^H$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_pthj1.*
XLabel=$p_\mathrm{T}^{Hj}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}^{Hj}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_pthjj12.*
XLabel=$p_\mathrm{T}^{Hjj}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_\mathrm{T}^{Hjj}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_centrality
XLabel=$\xi$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\xi$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_z_star
XLabel=$z^\ast$
YLabel=$\mathrm{d}\sigma / \mathrm{d}z^\ast$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_y_star
LogY=0
XLabel=$y^\ast$
YLabel=$\mathrm{d}\sigma / \mathrm{d}y^\ast$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_m_jj12.*
XLabel=$m_{jj}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}m_{jj}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_njets.*
XMinorTickMarks=0
XLabel=$N_\mathrm{jets}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}N_\mathrm{jets}$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_ht.*
XLabel=$H_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}H_\mathrm{T}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_xh.*
XLabel=$p_\mathrm{T}^H / H_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} (p_\mathrm{T}^H / H_\mathrm{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_x1.*
XLabel=$p_\mathrm{T}^{j_1} / H_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} (p_\mathrm{T}^{j_1} / H_\mathrm{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_x2.*
XLabel=$p_\mathrm{T}^{j_2} / H_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} (p_\mathrm{T}^{j_2} / H_\mathrm{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_x3.*
XLabel=$p_\mathrm{T}^{j_3} / H_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} (p_\mathrm{T}^{j_3} / H_\mathrm{T})$ [fb/GeV]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_delta_y_jj12.*
XLabel=$\Delta y_{jj}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\Delta y_{jj}$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_delta_r_jj12.*
XLabel=$\Delta R_{jj}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\Delta R_{jj}$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_delta_phi_jj12.*
LogY=0
XLabel=$\Delta \phi_{jj}$ [$\pi$ rad]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\Delta \phi_{jj}$ [fb/$\pi$ rad]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_log10_d12
XLabel=$\log_{10}(d_{12}/\mathrm{GeV})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\log_{10}(d_{12}/\mathrm{GeV})$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_log10_d23
XLabel=$\log_{10}(d_{23}/\mathrm{GeV})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\log_{10}(d_{23}/\mathrm{GeV})$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_log10_d34
XLabel=$\log_{10}(d_{34}/\mathrm{GeV})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\log_{10}(d_{34}/\mathrm{GeV})$ [fb]
# END PLOT

# BEGIN PLOT /MC_HJETSVBF/.*_log10_d45
XLabel=$\log_{10}(d_{45}/\mathrm{GeV})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\log_{10}(d_{45}/\mathrm{GeV})$ [fb]
# END PLOT
