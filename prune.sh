#!/usr/bin/bash -l

set -e

echo "Hello! It is now "`date '+%d %b %Y (%H:%M)'`

ENV=${1}
PHYSVAL_TOKEN=${2}
OUTPATH=${3}
GITURL=https://oauth2:${PHYSVAL_TOKEN}@gitlab.com/sherpa-team/validation/sherpa-physval.git

source ~/workspace/physval-prune/software/setup${ENV}
SCRIPT_LOC=$(dirname ${BASH_SOURCE[0]})
BASE=`realpath ${SCRIPT_LOC}`

function prune-branches-from-dir() {
  DIR=${1} # directory to be probed

  cd ${BASE}/
  # Assemble lists of local branches
  BRANCHES=$(find ${DIR} -type f -mtime +30 | cut -d"/" -f 2 \
                                            | cut -c 12- | rev \
                                            | cut -d "-" -f 3- | rev \
                                            | sort -u | grep -v "^master$") || true

  if [[ -z "${BRANCHES}" ]]; then return 0; fi # nothing to be done here
  # Assemble list of stale branches
  TRASH=$(git branch -r | awk '{print $1}' \
                        | egrep -v -f /dev/fd/0 <(echo ${BRANCHES} | tr ' ' '\n' | awk '{print "origin/"$1}') ) || true

  cd ${BASE}/
  # remove corresponding local files
  for YODA in `find ${DIR} -maxdepth 1 -type f -mtime +30`; do
    # disect file information
    #WHEN=$(echo ${YODA%%.*} | cut -c -10)
    #PROC=$(echo ${YODA%%.*} | rev | cut -d "-" -f 1 | rev)
    #HASH=$(echo ${YODA%%.*} | rev | cut -d "-" -f 2 | rev)
    TAG=$(echo ${YODA} | cut -c 12- | cut -d "-" -f 1)
    if [[ ${TAG} =~ ^v[0-9]+\.[0-9]+\.[0-9]+$ ]]; then continue; fi # protected for now
    BRANCH=$(echo ${YODA%%.*} | cut -c 12- | rev | cut -d "-" -f 3- | rev)
    if [[ "${BRANCH}" == "master" ]]; then continue; fi # protected for now
    RES=$(echo "${TRASH}" | grep "^origin/${BRANCH}$")  || true # check if branch is up for deletion
    if [[ ! -z "${RES}" ]]; then
      git rm ${DIR}/${YODA} --quiet
    fi
  done

  PHYSVAL_PUSH "remove ${DIR} from deleted branches"
}

cd ${BASE}/
git pull ${GITURL} 2>&1 --quiet

# First, prune remote branches
cd ${BASE}/software/sherpa
git fetch --quiet --prune origin

# Now prune files corresponding to stale branches
prune-branches-from-dir refs-commits
prune-branches-from-dir logs

# Now delete plotting directories older than a month
for plotdir in `find ${OUTPATH}/refs-commits/ -maxdepth 1 -type d -mtime +30`; do
  if [[ "`basename ${plotdir} | cut -c 12- | cut -d "-" -f 1`" == "master" ]]; then continue; fi
  rm -rf ${plotdir}
done
python ${BASE}/make-html ${OUTPATH}

echo "Done! It is now "`date '+%d %b %Y (%H:%M)'`
