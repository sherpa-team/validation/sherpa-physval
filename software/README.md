
# Installation instructions

[ToC]

## Git (if not already available on nodes)

```
wget https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.44.0.tar.gz
tar xzf git-2.44.0.tar.gz
cd git-2.44.0
make configure
./configure --prefix=${PWD}/local
make install -j4
cd ..
```

## Python virtual environment

```
python -m venv pyEnv
source pyEnv/bin/activate
pip install cython
pip install pyyaml
pip install requests
pip install matplotlib
pip install tqdm
pip install gitpython
```


## HepMC3

```
git clone ssh://git@gitlab.cern.ch:7999/hepmc/HepMC3.git
cd HepMC3
git checkout 3.2.6
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=${PWD}/../local -DHEPMC3_ENABLE_ROOTIO=OFF -DHEPMC3_ENABLE_PYTHON=OFF
make -j4 install
cd ../..
```

## FastJet

```
wget http://www.fastjet.fr/repo/fastjet-3.4.2.tar.gz
tar xzf fastjet-3.4.2.tar.gz
cd fastjet-3.4.2/
./configure --prefix=${PWD}/local --enable-shared --disable-auto-ptr --enable-allcxxplugins
make -j4 install
cd ..
```

## FJcontrib

```
wget http://cedar-tools.web.cern.ch/downloads/fjcontrib-1.051.tar.gz
tar xzf fjcontrib-1.051.tar.gz
cd fjcontrib-1.051/
./configure --fastjet-config=${PWD}/../fastjet-3.4.2/local/bin/fastjet-config CXXFLAGS=-fPIC
make fragile-shared-install
make -j4 install
cd ..
```

## HDF5 (currently not used)

```
wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.2/src/hdf5-1.14.2.tar.gz
tar xzf hdf5-1.14.2.tar.gz
cd hdf5-1.14.2/
./configure --prefix=${PWD}/local
make -j4 install
cd ..
export PATH=${PWD}/hdf5-1.14.2/local/bin:${PATH}
```

## YODA

```
git clone git@gitlab.com:hepcedar/yoda.git
cd yoda
autoreconf -vi
./configure --prefix=${PWD}/local
make -j4 install
cd ..
```

## Rivet

```
git clone git@gitlab.com:hepcedar/rivet.git
cd rivet/
autoreconf -vi
./configure --prefix=${PWD}/local --with-yoda=${PWD}/../yoda/local --with-hepmc3=${PWD}/../HepMC3/local --with-fastjet=${PWD}/../fastjet-3.4.2/local
make -j4 install
cd ..
```

## LHAPDF

```
git clone git@gitlab.com:hepcedar/lhapdf.git
cd lhapdf/
autoreconf -vi
./configure --prefix=${PWD}/local
make -j4 install
cd ..
```

## OpenLoops

```
git clone -b public_beta https://gitlab.com/openloops/OpenLoops.git
cd OpenLoops/
./scons
./openloops libinstall ppllj ppllj_ew ppllj2 pplljj pplljj_ew pplnjj pptt pptt_ew ppttj ppttj_ew pphjj_vbf ppjj ppjjj ppvj ppvv ppvvj
cd ..
```

## LibZip

```
wget https://libzip.org/download/libzip-1.8.0.tar.gz
tar xzf libzip-1.8.0.tar.gz
cd libzip-1.8.0/
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=${PWD}/../local
make -j4 install
cd ../..
```

## Sherpa

```
git clone git@gitlab.com:sherpa-team/sherpa.git
cd sherpa/
#mkdir build && cd build
#cmake .. --fresh -DCMAKE_INSTALL_PREFIX=${PWD}/../local -DRIVET_DIR=${PWD}/../../rivet/local/ -DHepMC3_DIR=${PWD}/../../HepMC3/local -DSHERPA_ENABLE_ANALYSIS=ON -DLibZip_DIR=${PWD}/../../libzip-1.8.0/local -DOPENLOOPS_DIR=${PWD}/../../OpenLoops -DLHAPDF_DIR=${PWD}/../../lhapdf/local
#make -j4 install
#cd ../..
```
