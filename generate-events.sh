#! /bin/bash

echo "Hello! It is now "`date '+%d %b %Y (%H:%M)'`

SCRIPT_LOC=$(dirname ${BASH_SOURCE[0]})
BASE=`realpath ${SCRIPT_LOC}`

# Print config info
Sherpa --version
echo "Using commit hash: "`git rev-parse HEAD`
rivet --version

CONFIG=${1}
RNG=${2}
SEED=$(python -c "import random; random.seed(98765); print(random.sample(range(10000,100000000), ${RNG}).pop())")

CMD="Sherpa ${CONFIG} RANDOM_SEED=${SEED} -A Analysis_${SEED}"
echo ${CMD}
$CMD || true

echo "Done! It is now "`date '+%d %b %Y (%H:%M)'`
