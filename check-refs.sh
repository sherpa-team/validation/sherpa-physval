#!/usr/bin/bash -l

set -e

echo "Hello! It is now "`date '+%d %b %Y (%H:%M)'`

SCRIPT_LOC=$(dirname ${BASH_SOURCE[0]})
BASE=`realpath ${SCRIPT_LOC}`

POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -o)
      OUTPATH="$2"
      shift
      shift
      ;;
    -s)
      SHERPA_TOKEN="$2"
      shift
      shift
      ;;
    -p)
      PHYSVAL_TOKEN="$2"
      shift
      shift
      ;;
    -m)
      MATCHEXPR="$2"
      shift
      shift
      ;;
    -l)
      PLOTLOG="$2"
      shift
      shift
      ;;
    -*|--*)
      echo "Unknown argument $1"
      exit 1
      ;;
    *)
      POISTIONAL_ARGS+=("$1")
      shift
      ;;
  esac
done
set -- "${POSITIONAL_ARGS[@]}" # restore positional arguments

#echo "OUTPATH=${OUTPATH}"
#echo "SHERPA_TOKEN=${SHERPA_TOKEN}"
#echo "PHYSVAL_TOKEN=${PHYSVAL_TOKEN}"
#echo "MATCHEXPR=${MATCHEXPR}"
#echo "PLOTLOG=${PLOTLOG}"

GITURL=https://oauth2:${PHYSVAL_TOKEN}@gitlab.com/sherpa-team/validation/sherpa-physval.git
#rivet --version

# See if there's any news ...
cd ${BASE}/
OLDHASH=$(git rev-parse HEAD)
#OLDHASH=3401285cf
if [[ -z ${MATCHEXPR} ]]; then
  git pull ${GITURL} --quiet
fi
NEWHASH=$(git rev-parse HEAD)
#NEWHASH=5fbfb8009
YODAS=
if [[ "${OLDHASH}" != "${NEWHASH}" ]]; then
  YODAS=$(git diff --name-status ${OLDHASH}..${NEWHASH} | grep "[A|M].*refs-c.*" | grep -o "refs-.*")
fi
if [[ ! -z ${MATCHEXPR} ]]; then
  YODAS=$(find refs-commits/ refs-tags/ -type f -regextype posix-extended -size +0 -regex ${MATCHEXPR})
fi
#YODAS=$(cat test.log | grep "create mode.*refs-c" | grep -o "refs-.*")
#echo "YODAS: $YODAS"

if [ -z "${YODAS}" ]; then
  echo "No new reference files: nothing to be done here. It is now "`date '+%d %b %Y (%H:%M)'`
else

  if [ ${PLOTLOG} ]; then
    rm -rf ${PLOTLOG} && touch ${PLOTLOG}
  fi
  for YODATYPE in ${YODAS[@]}; do
    if [ ! -s ${BASE}/${YODATYPE} ]; then
      continue # has zero size: only a placeholder
    fi

    OUTDIR="${OUTPATH}/${YODATYPE%%.*}"
    INFILE="${BASE}/${YODATYPE}"
    YODA=$(echo ${YODATYPE} | cut -d "/" -f 2)
    #DATE=$(echo ${YODA%%.*} | cut -d "-" -f -3)
    PROC=$(echo ${YODA%%.*} | rev | cut -d "-" -f 1 | rev)
    HASH=$(echo ${YODA%%.*} | rev | cut -d "-" -f 2 | rev)
    BRANCH=$(echo ${YODA%%.*} | cut -c 12- | rev | cut -d "-" -f 3- | rev)
    TITLE=${BRANCH}-${HASH}
    #SCALES="^ME.MUR=.*__ME.MUF=.*__ME.LHAPDF=93300"
    SCALES="^MUR=.*__MUF=.*__LHAPDF=93300"
    if [[ ${PROC} == *"higgs"* ]]; then
      SCALES="^MUR=.*__MUF=.*__LHAPDF=91900"
    fi
    CONFIG="Title=${TITLE}:BandComponentEnv=${SCALES}"

    REF=
    REF=$(find ${BASE}/refs-tags/ -type f -size +0 -regextype posix-extended -regex '.*-'${PROC}'\.yoda\.gz' | tail -n 1)
    if [ ${REF} ]; then
      REF=$(realpath ${REF})
      REFTITLE=$(basename ${REF} | cut -c 12- | cut -d "-" -f 1)
      REFCONFIG="Title=${REFTITLE}"
      if [[ ${REF} == *"v2."* ]]; then
        SCALES2="^(?!.*ASSEW).*"
        REFCONFIG="${REFCONFIG}:BandComponentEnv=${SCALES2}"
      else
        REFCONFIG="${REFCONFIG}:BandComponentEnv=${SCALES}"
      fi
      REF=${REF}:"${REFCONFIG}"
    fi

    # for feature branches, also plot latest master
    MASTER=
    if [[ ! -z "${BRANCH}" ]] && [[ "${BRANCH}" != "master" ]]; then
      TMP=$(python -c 'import sys; sys.path.insert(1, "'+${BASE}+'"); import physval_utils; physval_utils.print_ref("'+${PROC}+'")')
      if [[ ! -z "${TMP}" ]]; then
        TMP=$(realpath ${TMP})
        TMPNAME=$(basename ${TMP})
        TMPID=$(echo ${TMPNAME%%.*} | rev | cut -d "-" -f 2 | rev)
        TMPCONFIG="Title=master-${TMPID}"
        MASTER=${TMP}:"${TMPCONFIG}"
      fi
    fi

    echo "Plotting $YODA"
    #echo "OUTDIR: $OUTDIR"
    #echo "REF: $REF"
    #echo "MASTER: $MASTER"
    #echo "INFILE: $INFILE"
    #echo "CONFIG: $CONFIG"
    if [ ${PLOTLOG} ]; then
      echo "Plotting $YODA" >>${PLOTLOG}
      rivet-mkhtml -j 4 -o ${OUTDIR} --remove-options \
      ${REF} ${MASTER} ${INFILE}:"${CONFIG}" >>${PLOTLOG} 2>&1
    else
      rivet-mkhtml -j 4 -o ${OUTDIR} --remove-options \
      ${REF} ${MASTER} ${INFILE}:"${CONFIG}" 2>&1
    fi
    python ${BASE}/clean-index ${OUTDIR}/index.html

  done

  # Assemble top-level webpage
  python ${BASE}/make-html ${OUTPATH}

  # Check if there are open merge requests for the feature branches
  SEEN=""
  cd ${BASE}/software/sherpa
  git fetch --all --quiet || true
  for YODATYPE in ${YODAS[@]}; do

    YODA=$(echo ${YODATYPE} | cut -d "/" -f 2)
    BRANCH=$(echo ${YODA%%.*} | cut -c 12- | rev | cut -d "-" -f 3- | rev)

    # only consider feature branches, i.e. skip master branch
    if [[ -z "${BRANCH}" ]] || [[ "${BRANCH}" == "master" ]]; then continue; fi

    # check that there are no empty placeholders for this tag
    TAG=${BRANCH}-$(echo ${YODA%%.*} | rev | cut -d "-" -f 2 | rev)
    SKIP=false
    for f in $(ls ${BASE}/refs-commits/*-${TAG}-*); do
      if [ ! -s $f ]; then SKIP=true; break; fi
    done
    if ${SKIP}; then continue; fi

    # skip already seen branches
    if [[ "${SEEN}" == *"${BRANCH}"* ]]; then continue; fi

    # branch passes, consider it "seen"
    SEEN="$SEEN $BRANCH"

    # get latest hash (might be different from the one used to run the jobs)
    HASH=$(git log -n 1 --pretty=format:"%h" remotes/origin/${BRANCH})

    # try to find associated MR
    MR_ID=$(git ls-remote origin 'refs/merge-requests/*/head' | grep ${HASH} | cut -d "/" -f 3)
    if [[ -z "${MR_ID}" ]]; then
      continue # no associated MR, skip
    fi

    # prepare and send comment
    PROJECT_ID=5781712
    WHEN=$(echo ${YODA%%.*} | cut -c -10)
    HASH=$(echo ${YODA%%.*} | rev | cut -d "-" -f 2 | rev)
    MSG=$(python ${BASE}/check-diff ${BRANCH} ${HASH} ${WHEN})
    #echo $MSG

    curl --request POST "https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/${MR_ID}/notes" \
         --header "PRIVATE-TOKEN: ${SHERPA_TOKEN}" --header "Content-Type: application/json" \
         --data "{ \"body\": \"${MSG}\" }" >/dev/null 2>&1
  done

  echo "Done! It is now "`date '+%d %b %Y (%H:%M)'`
fi
