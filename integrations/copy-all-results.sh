#!/bin/bash

# might want to use existing paths?
for d in ~/Scratch/sherpaPhysVal/*; 
do
    fname=$d/Results.zip
    cp ${fname} $(echo $d | cut -d "-" -f 3).zip  || :
done
