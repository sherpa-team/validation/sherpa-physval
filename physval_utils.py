#!/usr/bin/env python

import os, re, gzip

BASE = os.path.dirname(os.path.realpath(__file__))

GROUP = {
  'lhc_zinc7'            : 'LHC Drell-Yan (7 TeV)',
  'lhc_zinc7_nompi'      : 'LHC Drell-Yan (7 TeV, no MPI)',
  'lhc_zinc13'           : 'LHC Drell-Yan (13 TeV)',
  'lhc_zjets13'          : 'LHC Z &plus; jets (13 TeV)',
  'lhc_ttbar13'          : 'LHC ttbar (13 TeV)',
  'lhc_ttjets13'         : 'LHC tt &plus; jets (13 TeV)',
  'lhc_higgs13_ggh'      : 'LHC ggF Higgs (13 TeV)',
  'lhc_higgs13_hjj'      : 'LHC Hjj (13 TeV)',
  'lhc_higgs13_vbfh'     : 'LHC VBF H (13 TeV)',
  'lhc_higgs13_vh'       : 'LHC VH (13 TeV)',
  'lhc_hq_enforced'      : 'LHC HQ (enf. spl.)',
  'lhc_hq_notenforced'   : 'LHC HQ (no enf. spl.)',
  'lhc_minbias7'         : 'LHC MinBias (7TeV)',
  'lhc_jets7'            : 'LHC Jets (7TeV)',
  'lhc_dijet7'           : 'LHC Dijets ME+PS (7TeV)',
  'lhc_dijet13'          : 'LHC Dijets ME+PS (13TeV)',
  'lhc_yyjets13'         : 'LHC yy &plus; jets (13 TeV)',
  'lhc_vbs13'            : 'LHC VVjj (13 TeV)',
  'lep_qcd'              : 'LEP QCD',
  'lep_photoprod_direct' : 'LEP &gamma; &plus; X (direct)',
  'lep_photoprod_double' : 'LEP &gamma; &plus; X (double)',
  'lep_photoprod_single' : 'LEP &gamma; &plus; X (single)',
  'lep_remnants_taupol'  : 'LEP &tau; pol.',
  'lep_remnants_isr'     : 'LEP ISR',
  'lep_remnants_noisr'   : 'LEP no ISR',
  'lep_remnants_epa'     : 'LEP EPA',
  'hera_remnants_dis'    : 'HERA DIS',
}

def find_refs(proc):
  return sorted([ f"{BASE}/refs-commits/"+f for f in os.listdir(f"{BASE}/refs-commits")
                  if re.search(r"\d{4}-\d{2}-\d{2}-master-\w{10}-%s.yoda.gz" % proc, f) and \
                  os.stat(f"{BASE}/refs-commits/"+f).st_size ])

def print_ref(proc):
  refs = find_refs(proc)
  if len(refs):
      print(refs[-1])


def parseLog(fileName):
    res = ''
    with gzip.open(fileName, 'rb') as f:

        log = str(f.read()).replace('\\n', '\n')

        has_unused = log.find('include subsettings that have not been used:')
        if has_unused >= 0:
            pos_start = has_unused + log[has_unused:].find(':') + 1
            pos_end = pos_start+log[pos_start:].find(r'For more details') - 2
            res += (f'\nUnused settings:\n```{log[pos_start:pos_end]}```\n')

        for idx, line in enumerate(re.findall(r'Error messages from .* exceeded frequency limit: .*\n', log)):
            if not idx:  res += ('\nSuppressed warnings/errors:\n')
            res += ('  - ' + line.replace(r"\'", "`"))

        has_fails = log.find(r'Momentum fail statistics:')
        if has_fails >= 0:
            res += ('\n| Component | Momentum fails |\n| :--- | ---: |\n')
            for idx, line in enumerate(re.findall(r'.*fails\n', log[has_fails:])):
                res += ('|' + re.sub(r'\ +\:', ' |', line.replace('fails\n', ' |\n')))

        summary = {}
        for line in re.findall(r'Warning in .*\n|Error in .*\n', log):
            trigger = line.replace('\n', '')
            summary[trigger] = summary.get(trigger, 0) + 1
        if len(summary):
            res += ('\n| Count | Warning/Error |\n| :---: | :--- |\n')
            # print summary content, sorted first by value, then key
            for k,v in sorted(summary.items(), key=lambda x: (-x[1], x[0])):
                res += (f'| {v} | `{k}` |\n')

    return res

